__author__ = 'Gaertner'

import cv2
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

import os
os.listdir(".")
os.getcwd()
os.chdir(r"D:\Users\Gaertner\Documents\Py_Skripte")

#this method simply reads the image off disk and then stores it as a multi-dimensional NumPy array
#img = cv2.imread('puzzle_small.jpg', 0)
#img = cv2.imread('wv2_layerstack_tif.tif', cv2.IMREAD_COLOR)

from osgeo import gdal
ds = gdal.Open("image/wv2_ndvi.tif")
img = np.array(ds.GetRasterBand(1).ReadAsArray())

#img_file = 'sin_waves1.tif'
#img = cv2.imread(img_file, cv2.IMREAD_COLOR)           # rgb
#alpha_img = cv2.imread(img_file, cv2.IMREAD_UNCHANGED) # rgba
#gray_img = cv2.imread(img_file, cv2.IMREAD_GRAYSCALE)  # grayscale
print img
print type(img)
#print 'RGB shape: ', img.shape        # Rows, cols, channels
#print 'ARGB shape:', alpha_img.shape
#print 'Gray shape:', gray_img.shape
#print 'img.dtype: ', img.dtype
#print 'img.size: ', img.size
#img = cv2.cvtColor(img, cv2.COLOR_BG2GRAY)
img2 = img.copy()
templateFileNamesList = os.listdir(r"template")
for i in templateFileNamesList:
    #print i

    NT = gdal.Open(r"template\\"+i)
    template = np.array(NT.GetRasterBand(1).ReadAsArray())
#template = cv2.imread('New Template.tif',cv2.IMREAD_COLOR)
    print template
# width and height
    print(template.shape)
    #w, h = template.shape[::-1]
    w = template.shape[0]
    h = template.shape[1]
    #print(w)
    #print(h)

# All the 6 methods for comparison in a list
    methods = ['cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF_NORMED', 'cv2.TM_CCOEFF_NORMED']

    for meth in methods:
        img = img2.copy()
        method = eval(meth)

    # Apply template Matching
        res = cv2.matchTemplate(img,template,method)

    # After the function finishes the comparison, the best matches can be found as global minimums (when CV_TM_SQDIFF was used) or maximums (when CV_TM_CCORR or CV_TM_CCOEFF was used) using the minMaxLoc() function.
    # In case of a color image, template summation in the numerator and each sum in the denominator is done over all of the channels and separate mean values are used for each channel.
    # That is, the function can take a color template and a color image. The result will still be a single-channel image, which is easier to analyze.
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)

        cv2.rectangle(img,top_left, bottom_right, 1, 9)

    #plt.subplot(121),plt.imshow(res,cmap='gray')
    #plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    #plt.subplot(122),plt.imshow(img,cmap='gray')
    #plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    #plt.suptitle(meth)

        im = Image.fromarray(res)
        im.save("result/" +i.split(".")[0]+ "_"+str(meth)+ ".tif")


    #plt.show()